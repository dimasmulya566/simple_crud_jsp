/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud.jsp.model.koneksi;

import crud.jsp.model.UserEntity;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author USER
 */
public class Koneksi {
    
     public Connection bukaKoneksi() throws SQLException {
    Connection connect = null;
    try {
        Class.forName("org.postgresql.Driver");
        connect = DriverManager.getConnection("jdbc:postgresql://localhost:5432/crud_jsp", "postgres", "postgres");
    } catch (ClassNotFoundException | SQLException e) {
        e.printStackTrace();
        // Tangani pengecualian sesuai kebutuhan, seperti melemparkan pengecualian atau menampilkan pesan kesalahan
    }
    return connect;
}

      
      protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    // Membuat objek Koneksi
    Koneksi koneksi = new Koneksi();
String INSERT_USERS_SQL = "INSERT INTO user_management" + "  (name, email, address) VALUES "
            + " (?, ?, ?);";
    try {
        // Mendapatkan koneksi
        Connection connection = koneksi.bukaKoneksi();
   PreparedStatement pr = connection.prepareStatement(INSERT_USERS_SQL);
            pr.setString(1, "name");
            pr.setString(2, "email");
            pr.setString(3, "address");
            
            pr.executeUpdate();
        // Lakukan operasi CRUD dengan koneksi yang telah didapatkan
        // ...
    } catch (SQLException e) {
        e.printStackTrace();
        // Tangani pengecualian sesuai kebutuhan
    }
}


//    private String jdbcURL = "jdbc:mysql://localhost:3306/crud_jsp?useSSL=false";
//    private String jdbcUsername = "root";
//    private String jdbcPassword = "root";
//
//    private static final String INSERT_USERS_SQL = "INSERT INTO user_management" + "  (name, email, address) VALUES "
//            + " (?, ?, ?);";
//
//    private static final String SELECT_USER_BY_ID = "select id,name,email,address from user_management where id =?";
//    private static final String SELECT_ALL_USERS = "select * from user_management";
//    private static final String DELETE_USERS_SQL = "delete from user_management where id = ?;";
//    private static final String UPDATE_USERS_SQL = "update user_management set name = ?,email= ?, address =? where id = ?;";
//
//    protected Connection getConnection() {
//        Connection connection = null;
//        try {
//            Class.forName("com.mysql.jdbc.Driver");
//            connection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
//        } catch (SQLException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        } catch (ClassNotFoundException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//        return connection;
//    }
//
//    public void insertTable(UserEntity userEntity) throws SQLException {
//        try (Connection con = getConnection();
//                PreparedStatement pr = con.prepareStatement(INSERT_USERS_SQL);) {
//            pr.setString(1, userEntity.getName());
//            pr.setString(2, userEntity.getEmail());
//            pr.setString(3, userEntity.getAddress());
//            pr.executeUpdate();
//        } catch (SQLException e) {
//            printSQLException(e);
//        }
//    }
//
//    public UserEntity selectById(int id) throws SQLException {
//        UserEntity user = null;
//        try (Connection con = getConnection();
//                PreparedStatement pr = con.prepareStatement(SELECT_USER_BY_ID);) {
//            pr.setInt(1, id);
//            System.out.println(pr);
//            ResultSet set = pr.executeQuery();
//
//            while (set.next()) {
//                String name = set.getString("name");
//                String email = set.getString("email");
//                String address = set.getString("address");
//                user = new UserEntity(id, name, email, address);
//            }
//        } catch (SQLException e) {
//            printSQLException(e);
//        }
//        return user;
//    }
//
//    public List<UserEntity> selectAll() throws SQLException {
//        List<UserEntity> userList = new ArrayList<>();
//        try (Connection con = getConnection();
//            PreparedStatement pr = con.prepareStatement(SELECT_ALL_USERS);)
//            {
//                ResultSet set = pr.executeQuery();
//                
//                while(set.next()){
//                    int id = set.getInt("id");
//                      String name = set.getString("name");
//                String email = set.getString("email");
//                String address = set.getString("address");
//                userList.add(new UserEntity(id, name, email, address));
//                }
//        }catch(SQLException e){
//            printSQLException(e);
//        }
//            return userList;
//    }
//    
//    public boolean deleteUser(int id) throws SQLException {
//		boolean rowDeleted;
//		try (Connection connection = getConnection();
//				PreparedStatement statement = connection.prepareStatement(DELETE_USERS_SQL);) {
//			statement.setInt(1, id);
//			rowDeleted = statement.executeUpdate() > 0;
//		}
//		return rowDeleted;
//	}
//
//	public boolean updateUser(UserEntity user) throws SQLException {
//		boolean rowUpdated;
//		try (Connection connection = getConnection();
//				PreparedStatement statement = connection.prepareStatement(UPDATE_USERS_SQL);) {
//			statement.setString(1, user.getName());
//			statement.setString(2, user.getEmail());
//			statement.setString(3, user.getAddress());
//			statement.setInt(4, user.getId());
//
//			rowUpdated = statement.executeUpdate() > 0;
//		}
//		return rowUpdated;
//	}
//    
//    
//  
//    
//
//    private void printSQLException(SQLException ex) {
//        for (Throwable e : ex) {
//            if (e instanceof SQLException) {
//                e.printStackTrace(System.err);
//                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
//                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
//                System.err.println("Message: " + e.getMessage());
//                Throwable t = ex.getCause();
//                while (t != null) {
//                    System.out.println("Cause: " + t);
//                    t = t.getCause();
//                }
//            }
//        }
//    }

}

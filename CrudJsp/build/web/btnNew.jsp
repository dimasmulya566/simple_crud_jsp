<!DOCTYPE html>
<html>
    <head>
        <title>Header - Selamat datang di Test Admin</title>
        <style>
            body {
                background-color: white;
            }
            .form-container {
                width: 400px;
                margin: 0 auto;
                text-align: center;
                padding: 20px;
                border: 1px solid blue;
                border-radius: 5px;
                background-color: #f1f1f1;
            }

            .form-container label {
                display: inline-block;
                text-align: left;
                width: 100px;
            }

            .form-container input[type="text"],
            .form-container input[type="email"],
            .form-container textarea {
                width: 200px;
                padding: 5px;
                margin: 5px 0;
                border: 1px solid #ccc;
                border-radius: 3px;
            }

            .form-container input[type="submit"] {
                width: 100px;
                padding: 5px;
                margin-top: 10px;
                background-color: blue;
                color: white;
                border: none;
                border-radius: 3px;
                cursor: pointer;
            }
        </style>
    </head>
    <body>
        <div class="form-container">
            <h2>Formulir</h2>
            <form action="saveData.jsp" method="post">
                <label for="name">Name:</label>
                <input type="text" name="name" id="name" required><br>

                <label for="email">Email:</label>
                <input type="email" name="email" id="email" required><br>

                <label for="address">Address:</label>
                <textarea name="address" id="address" rows="4" required></textarea><br>

                <label for="saldo">Saldo:</label>
                <input type="text" name="saldo" id="saldo" required><br>

                <input type="submit" value="Save">
            </form>
        </div>
    </body>
</html>

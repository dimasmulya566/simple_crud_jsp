<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="javax.servlet.http.HttpServletResponse"%>
<%@ page import="crud.jsp.model.koneksi.Koneksi" %>

<%
    try {
        // Membuat objek Koneksi
        Koneksi koneksi = new Koneksi();

        // Mendapatkan koneksi
        Connection connection = koneksi.bukaKoneksi();

        // Mendapatkan data dari parameter form
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String address = request.getParameter("address");
        String saldoStr = request.getParameter("saldo");
        int saldo = Integer.parseInt(saldoStr);
        // ...

        PreparedStatement statement = null;
        try {
            // Menyiapkan pernyataan SQL
            String sql = "INSERT INTO users_management (name, email, address, saldo) VALUES (?, ?, ?, ?)";
            statement = connection.prepareStatement(sql);

            // Mengatur parameter pada pernyataan SQL
            statement.setString(1, name);
            statement.setString(2, email);
            statement.setString(3, address);
            statement.setInt(4, saldo);
            // ...

            // Menjalankan pernyataan SQL
            int rowsInserted = statement.executeUpdate();

            if (rowsInserted > 0) {
                out.println("Data berhasil disimpan.");
                response.sendRedirect("index.jsp");
            } else {
                out.println("Gagal menyimpan data.");
            }

            // Redirect ke index.jsp setelah penyimpanan berhasil
//      response.sendRedirect("index.jsp");
//      HttpServletResponse httpResponse = (HttpServletResponse) response;
//      httpResponse.sendRedirect("index.jsp");
        } catch (SQLException e) {
            e.printStackTrace();
            // Tangani pengecualian sesuai kebutuhan
        } finally {
            // Menutup pernyataan
            if (statement != null) {
                statement.close();
            }

            // Menutup koneksi
            if (connection != null) {
                connection.close();
            }
        }
    } catch (Exception e) {
        e.printStackTrace();
        // Tangani pengecualian sesuai kebutuhan
    }
%>

<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="crud.jsp.model.koneksi.Koneksi" %>

<!DOCTYPE html>
<html>
    <head>
        <title>Edit Data</title>
        <style>
            body {
                background-color: #f1f1f1;
                font-family: Arial, sans-serif;
            }

            .container {
                max-width: 400px;
                margin: 0 auto;
                padding: 20px;
                background-color: #fff;
                border: 1px solid #ddd;
                border-radius: 5px;
                box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
            }

            .container h2 {
                text-align: center;
                margin-bottom: 20px;
            }

            .form-group {
                margin-bottom: 20px;
            }

            .form-group label {
                display: block;
                font-weight: bold;
                margin-bottom: 5px;
            }

            .form-group input[type="text"] {
                width: 100%;
                padding: 8px;
                border: 1px solid #ccc;
                border-radius: 4px;
            }

            .form-group input[type="submit"] {
                background-color: #4CAF50;
                color: white;
                border: none;
                padding: 10px 20px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                border-radius: 4px;
                cursor: pointer;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <h2>Edit Data</h2>

            <%
                String name = request.getParameter("name");
                Koneksi koneksi = new Koneksi();
                Connection connection = koneksi.bukaKoneksi();
                PreparedStatement statement = null;
                ResultSet resultSet = null;

                try {
                    // Mendapatkan data yang akan diedit dari database berdasarkan name
                    String sql = "SELECT name, email, address, saldo FROM users_management WHERE name = ?";
                    statement = connection.prepareStatement(sql);
                    statement.setString(1, name);
                    resultSet = statement.executeQuery();

                    if (resultSet.next()) {
                        String email = resultSet.getString("email");
                        String address = resultSet.getString("address");
            %>

            <form action="update.jsp" method="post">
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="text" name="email" value="<%= email%>">
                </div>

                <div class="form-group">
                    <label for="address">Address:</label>
                    <input type="text" name="address" value="<%= address%>">
                </div>

                <div class="form-group">
                    <input type="hidden" name="name" value="<%= name%>">
                    <input type="submit" value="Update">
                </div>
            </form>

            <%
                    } else {
                        out.println("<p>Data tidak ditemukan.</p>");
                    }
                } catch (Exception e) {
                    out.println("<p>Terjadi kesalahan: " + e.getMessage() + "</p>");
                } finally {
                    // Tutup koneksi dan sumber daya
                    if (resultSet != null) {
                        try {
                            resultSet.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (statement != null) {
                        try {
                            statement.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (connection != null) {
                        try {
                            connection.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            %>
        </div>

    </body>
</html>

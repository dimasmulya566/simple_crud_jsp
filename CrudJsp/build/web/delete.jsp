<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.SQLException"%>
<%@page import="crud.jsp.model.koneksi.Koneksi"%>
<%@ page import="java.io.*,java.util.*"%>

<%
    // Mendapatkan nilai parameter dari URL
    String name = request.getParameter("name");

    // Membuat koneksi ke database
    Connection connection = null;
    PreparedStatement statement = null;
    try {
        Koneksi koneksi = new Koneksi();
        connection = koneksi.bukaKoneksi(); // Menggunakan koneksi yang telah Anda buat
        String deleteQuery = "DELETE FROM users_management WHERE name = ?";
        statement = connection.prepareStatement(deleteQuery);
        statement.setString(1, name);
        statement.executeUpdate();

        // Redirect ke halaman sukses setelah penghapusan berhasil
        response.sendRedirect("index.jsp");
    } catch (SQLException e) {
        e.printStackTrace();
        // Redirect ke halaman error jika terjadi kesalahan dalam penghapusan
        response.sendRedirect("error.jsp");
    } finally {
        // Menutup koneksi dan statement
        try {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
%>

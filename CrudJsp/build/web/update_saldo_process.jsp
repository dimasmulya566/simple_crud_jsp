<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.SQLException"%>
<%@page import="crud.jsp.model.koneksi.Koneksi"%>
<%@ page import="java.io.*,java.util.*"%>

<%
    // Mendapatkan nilai parameter dari formulir
    String name = request.getParameter("name");
    int saldo = Integer.parseInt(request.getParameter("saldo"));

    // Membuat koneksi ke database
    Connection connection = null;
    PreparedStatement statement = null;
    try {
        Koneksi koneksi = new Koneksi();
        connection = koneksi.bukaKoneksi(); // Menggunakan koneksi yang telah Anda buat
        String updateQuery = "UPDATE users_management SET saldo = saldo + ? WHERE name = ?";
        statement = connection.prepareStatement(updateQuery);
        statement.setInt(1, saldo);
        statement.setString(2, name);
        statement.executeUpdate();

        // Redirect ke halaman sukses setelah pembaruan berhasil
        response.sendRedirect("index.jsp");
    } catch (SQLException e) {
        e.printStackTrace();
        // Redirect ke halaman error jika terjadi kesalahan dalam pembaruan
        response.sendRedirect("error.jsp");
    } finally {
        // Menutup koneksi dan statement
        try {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
%>

<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="javax.servlet.http.HttpServletResponse"%>
<%@ page import="crud.jsp.model.koneksi.Koneksi" %>

<!DOCTYPE html>
<html>
    <head>
        <title>Header - Selamat datang di Test Admin</title>
        <style>
            body {
                background-color: white;
            }
            .table-container {
                margin-top: 20px;
            }

            .header {
                text-align: center;
                padding: 20px;
                background-color: blue;
                color: white;
            }

            .new-button {
                background-color: red;
                color: white;
                border: none;
                padding: 10px 20px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                margin-top: 20px;
                cursor: pointer;
            }

            table {
                width: 100%;
                border-collapse: collapse;
            }

            th {
                padding: 8px;
                text-align: left;
                border-bottom: 1px solid #ddd;
            }

            th {
                background-color: blue;
                color: white;
            }
        </style>
    </head>
    <body>
        <div class="header">
            <h2>Selamat datang di Test Admin</h2>
        </div>
        <a href="btnNew.jsp" class="new-button">New</a>
        <div class="table-container">
            <table>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Saldo</th>
                    <th>Action</th>
                </tr>
                <%
                    Koneksi koneksi = new Koneksi();
                    Connection connection = koneksi.bukaKoneksi();
                    String sql = "SELECT name, email, address, saldo FROM users_management";
                    PreparedStatement statement = connection.prepareStatement(sql);
                    ResultSet resultSet = statement.executeQuery();
                    while (resultSet.next()) {
                        String name = resultSet.getString("name");
                        String email = resultSet.getString("email");
                        String address = resultSet.getString("address");
                        String saldoStr = resultSet.getString("saldo");

                        double saldo = Double.parseDouble(saldoStr);

                        NumberFormat format = NumberFormat.getCurrencyInstance(new Locale("id", "ID"));
                        String saldoFormatted = format.format(saldo);
                        out.println("<tr>");
                        out.println("<td>" + name + "</td>");
                        out.println("<td>" + email + "</td>");
                        out.println("<td>" + address + "</td>");
                        out.println("<td>" + saldoFormatted + "</td>");
                        out.println("<td><a href=\"edit.jsp?name=" + name + "\">Edit</a> | <a href=\"delete.jsp?name=" + name + "\">Delete</a> | <a href=\"update_saldo.jsp?name=" + name + "\">Update Saldo</a></td>");
                        out.println("</tr>");
                    }
                    // Tutup koneksi dan sumber daya
                    resultSet.close();
                    statement.close();
                    connection.close();
                %>
            </table>
        </div>
    </body>
</html>

<!DOCTYPE html>
<html>
    <head>
        <title>Update Saldo</title>
        <style>
            body {
                background-color: white;
                display: flex;
                justify-content: center;
                align-items: center;
                height: 100vh;
            }

            .container {
                background-color: #f5f5f5;
                padding: 20px;
                border-radius: 5px;
                box-shadow: 0 2px 5px rgba(0, 0, 0, 0.3);
                max-width: 400px;
                width: 100%;
            }

            .form-group {
                margin-bottom: 20px;
            }

            .form-group label {
                display: block;
                font-weight: bold;
            }

            .form-group input {
                width: 100%;
                padding: 10px;
                border: 1px solid #ddd;
                border-radius: 3px;
            }

            .form-group button {
                background-color: blue;
                color: white;
                border: none;
                padding: 10px 20px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                cursor: pointer;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <h2>Update Saldo</h2>
            <form action="update_saldo_process.jsp" method="post">
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" id="name" name="name" readonly value="<%= request.getParameter("name")%>">
                </div>
                <div class="form-group">
                    <label for="saldo">Saldo:</label>
                    <input type="number" id="saldo" name="saldo" required>
                </div>
                <div class="form-group">
                    <button type="submit">Update</button>
                </div>
            </form>
        </div>
    </body>
</html>

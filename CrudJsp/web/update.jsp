<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="crud.jsp.model.koneksi.Koneksi" %>

<!DOCTYPE html>
<html>
    <head>
        <title>Update Data</title>
    </head>
    <body>
        <%
            String name = request.getParameter("name");
            String email = request.getParameter("email");
            String address = request.getParameter("address");
            Koneksi koneksi = new Koneksi();
            Connection connection = koneksi.bukaKoneksi();
            PreparedStatement statement = null;

            try {
                // Update data dalam database berdasarkan name
                String sql = "UPDATE users_management SET email = ?, address = ? WHERE name = ?";
                statement = connection.prepareStatement(sql);
                statement.setString(1, email);
                statement.setString(2, address);
                statement.setString(3, name);

                int rowsUpdated = statement.executeUpdate();

                if (rowsUpdated > 0) {
        %>

        <h2>Data berhasil diperbarui.</h2>
        <p><a href="index.jsp">Kembali ke halaman utama</a></p>

        <%
                } else {
                    out.println("<h2>Gagal memperbarui data.</h2>");
                    out.println("<p><a href=\"edit.jsp?name=" + name + "\">Kembali ke halaman edit</a></p>");
                }
            } catch (Exception e) {
                out.println("<h2>Terjadi kesalahan: " + e.getMessage() + "</h2>");
                out.println("<p><a href=\"edit.jsp?name=" + name + "\">Kembali ke halaman edit</a></p>");
            } finally {
                // Tutup koneksi dan sumber daya
                if (statement != null) {
                    try {
                        statement.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        %>
    </body>
</html>
